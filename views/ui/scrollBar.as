package views.ui
{
	import fl.controls.UIScrollBar

	public class scrollBar extends UIScrollBar
	{
		public function scrollBar():void
		{
			renderStyles();
		}

		private function renderStyles(){
			setStyle('trackUpSkin', EmptySkyn);
			setStyle('downArrowDisabledSkin', EmptySkyn);
			setStyle('downArrowDownSkin', EmptySkyn);
			setStyle('downArrowOverSkin', EmptySkyn);
			setStyle('downArrowUpSkin', EmptySkyn);
			setStyle('upArrowDisabledSkin', EmptySkyn);
			setStyle('upArrowDownSkin', EmptySkyn);
			setStyle('upArrowOverSkin', EmptySkyn);
			setStyle('upArrowUpSkin', EmptySkyn);
			setStyle('thumbIcon', ScrollThumb);
			setStyle('thumbDisabledSkin', EmptySkyn);
			setStyle('thumbDownSkin', EmptySkyn);
			setStyle('thumbOverSkin', EmptySkyn);
			setStyle('thumbUpSkin', EmptySkyn);
			setStyle('trackDisabledSkin', EmptySkyn);
			setStyle('trackDownSkin', EmptySkyn);
			setStyle('trackOverSkin', EmptySkyn);
			setStyle('trackUpSkin', EmptySkyn);
		}

	}
}