package views
{
    import flash.display.MovieClip;
    import flash.events.Event;

    import com.greensock.*;
    import com.greensock.easing.*;
    import com.greensock.plugins.*;

    import fl.events.ScrollEvent;
    import views.ui.scrollBar;

    public class BioMovie extends MovieClip
    {
        private var main:MovieClip;
        public var sb:scrollBar = new scrollBar();
        public var scroll:Number = 0;

        public function BioMovie()
        {
            main = loaderInfo.content as MovieClip;
            this.addEventListeners();
        }

        public function addEventListeners()
        {
            main.addEventListener('clickBioBtn', this.clickOnMe);
            CloseBtn.addEventListener('click', clickClose);
        };

        public function clickOnMe(e:Event):void
        {
            TweenLite.to(CloseBtn, 0, {autoAlpha:0});
            renderScroll();
            animateEntry();
        };

        public function renderScroll():void{

            texto.mask = bioMask;
            sb.x = bioMask.x+bioMask.width/2+15;
            sb.y = -bioMask.height/2 - 110;
            sb.height = bioMask.height;
            sb.enabled = true;
            sb.setScrollProperties(texto.height, 0, (texto.height-bioMask.height));
            sb.addEventListener(ScrollEvent.SCROLL, mouseScroll);
            addChild(sb);
        }

        private function mouseScroll(e:ScrollEvent):void{
            texto.y = -e.position + bioMask.y;
        }

        public function animateEntry( ):void
        {
            TweenMax.fromTo(this, 1, {autoAlpha:0, y:this.y}, {autoAlpha:1, y:(stage.stageHeight/2+50)});
            TweenLite.to(main.SubTitle, 1, {autoAlpha:0, onComplete:showTitle});
        };

        private function showTitle( ):void
        {
            main.SubTitle.title.text = 'Biography';
            TweenMax.fromTo(main.SubTitle, 1.5, {y:main.SubTitle.y-10, autoAlpha:0,blurFilter:{blurX:20, blurY:20}}, {y:main.SubTitle.y, autoAlpha:1, blurFilter:{blurX:0, blurY:0}});
            TweenMax.fromTo(CloseBtn, 1, {y:CloseBtn.y+100}, {y:CloseBtn.y, autoAlpha:1, delay:1});
        };


        public function clickClose( e:Event ):void
        {
            animateClose();
        };

        public function animateClose( ):void
        {
            TweenMax.to(this, 1, {autoAlpha:1, y:stage.stageHeight+height});
            TweenMax.allTo([CloseBtn], 1, {autoAlpha:0});
            TweenLite.to(main.SubTitle, 1, {autoAlpha:0});
            main.dispatchEvent(new Event('closeBioMovie'));
        };





    }
}